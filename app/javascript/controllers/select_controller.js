import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js";

export default class extends Controller {
    static targets = ["selectedCountry"]

    async loadCities(e) {
        const response = await get(`/select_box/load_cities`, {
            query: {
                selected_country: this.selectedCountryTarget.value,
            },
            responseKind: "turbo-stream",
        });
    }
}
