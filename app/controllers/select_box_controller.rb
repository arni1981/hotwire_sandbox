class SelectBoxController < ApplicationController
  def index

  end

  def load_cities
    if params[:selected_country] == 'USA'
      @cities = ['New York', 'Houston']
    elsif params[:selected_country] == 'Spain'
      @cities = ['Barcelona', 'Madrid']
    end
  end
end
